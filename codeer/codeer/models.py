from .pathfinder import test_input_upload_path, test_output_upload_path, submition_upload_path
from .user_models import *
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.files.storage import FileSystemStorage

storage = FileSystemStorage(settings.STORAGE)


class Task(models.Model):
    title = models.CharField(max_length=254)
    description = models.CharField(max_length=1000)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)


class Test(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='tests')
    input = models.FileField(upload_to=test_input_upload_path, storage=storage)
    output = models.FileField(upload_to=test_output_upload_path, storage=storage)
    public = models.BooleanField(default=False)


class Language(models.Model):
    title = models.CharField(max_length=100)
    runner = models.CharField(max_length=200)
    file_extension = models.CharField(max_length=10)
    compile = models.CharField(max_length=256, null=True)
    container = models.CharField(max_length=200, default='pytest')

    def __str__(self):
        return self.title


class Configuration(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='configurations')
    language = models.ForeignKey(Language, on_delete=models.CASCADE, related_name='+')
    time_limit = models.FloatField(validators=[MinValueValidator(0.5), MaxValueValidator(10)])
    memory_limit = models.IntegerField(validators=[MinValueValidator(8), MaxValueValidator(512)])

    def __str__(self):
        return f'{self.language.title} - {self.time_limit}s - {self.memory_limit}m'


class Submition(models.Model):
    OK = 'OK'
    WA = 'WA'
    TL = 'TL'
    ML = 'ML'
    RE = 'RE'
    CE = 'CE'
    TST = '__'
    RESULT_CHOICES = (
        (OK, 'OK'), (WA, 'Wrong answer'), (TL, 'Time limit exceeded'), (RE, 'Runtime error'),
        (CE, 'Compilation error'), (TST, 'Testing')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='submitions')
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='submitions')
    solution = models.FileField(upload_to=submition_upload_path, storage=storage)
    result = models.CharField(max_length=2, choices=RESULT_CHOICES, default=TST)
    message = models.CharField(max_length=10000, null=True)
    configuration = models.ForeignKey(Configuration, on_delete=models.CASCADE, related_name='submitions')
    elapsed_time = models.FloatField(default=0)
