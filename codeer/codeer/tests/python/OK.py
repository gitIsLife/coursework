q = int(input())
res = 1
while q > 1:
    res *= q
    q -= 1

print(res)