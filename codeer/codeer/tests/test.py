from django.test import TestCase, override_settings
from ..models import *
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from ..task_runner import run_submition_, prepare_arguments


@override_settings(STORAGE='test_data')
class TaskRunnerTestPy(TestCase):
    def setUp(self):
        lang_py = Language.objects.create(title='py', compile=None, file_extension='py', runner='python3 sol.py',
                                          container='pytest')
        self._task = task = Task.objects.create(title='1', description='1')
        Test.objects.create(task=task, input=SimpleUploadedFile('q.txt', '3'.encode()),
                            output=SimpleUploadedFile('2.txt', '6'.encode()))
        Test.objects.create(task=task, input=SimpleUploadedFile('1.txt', '14'.encode()),
                            output=SimpleUploadedFile('qw.txt', '87178291200'.encode()))
        self._conf = Configuration.objects.create(task=task, language=lang_py, time_limit=1, memory_limit=128)
        self._u = User.objects.create_user('q', 'w', 'e')

    def test_python_OK(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/python/OK.py',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'OK')
        self.assertIsNone(result[1])

    def test_python_WA(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/python/WA.py',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'WA')
        self.assertIsNone(result[1])
        self.assertIsNone(result[1])

    def test_python_RE(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/python/RE.py',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'RE')
        self.assertIsNone(result[1])

    def test_python_TL(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/python/TL.py',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'TL')
        self.assertIsNone(result[1])

    def test_python_ML(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/python/ML.py',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'ML')


@override_settings(STORAGE='test_data')
class TaskRunnerTestCpp(TestCase):
    def setUp(self):
        lang_c = Language.objects.create(title='cpp', compile='g++-7 -O2 sol.cpp', file_extension='cpp',
                                         runner='./a.out', container='cpptest')
        self._task = task = Task.objects.create(title='1', description='1')
        Test.objects.create(task=task, input=SimpleUploadedFile('q.txt', '3'.encode()),
                            output=SimpleUploadedFile('2.txt', '6'.encode()))
        Test.objects.create(task=task, input=SimpleUploadedFile('1.txt', '14'.encode()),
                            output=SimpleUploadedFile('qw.txt', '87178291200'.encode()))
        self._conf = Configuration.objects.create(task=task, language=lang_c, time_limit=1, memory_limit=128)
        self._u = User.objects.create_user('q', 'w', 'e')

    def test_cpp_OK(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/cpp/OK.cpp',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'OK')
        self.assertIsNone(result[1])

    def test_cpp_WA(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/cpp/WA.cpp',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'WA')
        self.assertIsNone(result[1])

    def test_cpp_RE(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/cpp/RE.cpp',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'RE')
        self.assertIsNone(result[1])

    def test_cpp_TL(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/cpp/TL.cpp',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'TL')
        self.assertIsNone(result[1])

    def test_cpp_ML(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/cpp/ML.cpp',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'ML')
        self.assertIsNone(result[1])

    def test_cpp_CE(self):
        subm = Submition.objects.create(task=self._task, configuration=self._conf, user=self._u,
                                        solution=SimpleUploadedFile('2.py', open('codeer/tests/cpp/CE.cpp',
                                                                                 'r').read().encode()))
        result = run_submition_(*prepare_arguments(subm))
        self.assertEqual(result[0], 'CE')
        self.assertIsNotNone(result[1])
