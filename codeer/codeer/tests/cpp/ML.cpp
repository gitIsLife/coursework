#include <iostream>
#include <vector>

int main() {
    int q;
    std::cin >> q;
    uint64_t res = 1;
    std::vector<uint64_t> vect(q);
    while(q>1){
        res *= q--;
        vect.resize(vect.size() * res);
    }
    std::cout << res;
}