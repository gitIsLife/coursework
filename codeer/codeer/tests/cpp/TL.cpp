#include <iostream>

int main() {
    int q;
    std::cin >> q;
    uint64_t res = 1;
    while(res>=0){
        res *= q--;
    }
    std::cout << res;
}