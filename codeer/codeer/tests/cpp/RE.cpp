#include <iostream>

int main() {
    int q;
    std::cin >> q;
    uint64_t res = 1;
    while(q){
        res *= q--;
    }
    std::cout << res / q;
}