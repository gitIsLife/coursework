from django.forms import ModelForm, HiddenInput, FileField, ClearableFileInput
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Test, Submition, Profile, Task, Configuration
from django import forms


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description', 'owner']
        widgets = {'description': forms.Textarea(), 'owner': HiddenInput()}


class TestForm(ModelForm):
    class Meta:
        model = Test
        fields = '__all__'
        widgets = {'task': HiddenInput()}


class SubmitionForm(ModelForm):
    solution = FileField(required=False)

    class Meta:
        model = Submition
        exclude = ['result', 'message', 'elapsed_time']
        widgets = {'task': HiddenInput(), 'user': HiddenInput(), 'solution': ClearableFileInput()}


class UserRegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image']


class ConfigurationCreateForm(ModelForm):
    class Meta:
        model = Configuration
        fields = ['time_limit', 'memory_limit', 'language', 'task']
        widgets = {'task': HiddenInput}
