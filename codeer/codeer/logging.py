import logging

NAME = 'log'


def set_up():
    logger = get_logger()
    fh = logging.FileHandler('codeer.log')
    fh.setLevel(logging.ERROR)
    logger.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    logger.addHandler(ch)


def get_logger(name=NAME):
    return logging.getLogger(name)


def log(message):
    get_logger().info(message)


def log_ex(ex):
    get_logger().exception(str(ex))
