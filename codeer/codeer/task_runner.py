from subprocess import PIPE, Popen, TimeoutExpired
from .process_pool import ProcessPool
from .logging import log, log_ex
import os
import time
from django.conf import settings


def prepare_task(submition, language, configuration):
    memory_str = f'{configuration.memory_limit}m'
    p = Popen(args=('docker', 'run', '-d', '--memory', memory_str, '--memory-swap',
                    memory_str, '-t', language.container), stdout=PIPE)
    out, err = p.communicate()
    cid = out.decode().strip('\n')
    p = Popen(f'docker cp {submition.solution.path} {cid}:sol.{language.file_extension}', shell=True)
    p.wait()
    return cid


def compile_task(language, cid):
    if language.compile:
        p = Popen(f'docker exec {cid} sh -c "{language.compile}"', shell=True, stderr=PIPE, universal_newlines=True)
        out, err = p.communicate()
        log(f'COMPILE RETCODE:{p.returncode}, OUT:{out}, ERR:{err}')
        if p.returncode:
            return err


def prepare_test(test, cid):
    p = Popen(f'docker cp {test.input.path} {cid}:input.txt', shell=True)
    p.wait()
    log(f'CP INPUT:  {p.returncode}')
    p = Popen(f'docker exec {cid} sh -c "test -e output.txt && rm output.txt; touch output.txt"', shell=True)
    p.wait()
    log(f'RM OLD OUTPUT:  {p.returncode}')


def run_test(cid, configuration, language):
    log(f'STARTING TEST:')
    os.nice(-20)
    try:
        beg = int(round(time.time() * 1000))
        p = Popen(f'docker exec {cid} sh -c "/usr/bin/time -f \'%U\' 2>&1 {language.runner} < input.txt > output.txt"',
                  shell=True, stderr=PIPE, stdout=PIPE, universal_newlines=True)
        p.wait(configuration.time_limit * 1.5 + 3)
        end = int(round(time.time() * 1000))
        log(f'ELAPSED TIME____python: {end - beg}ms')
        out, err = p.communicate()
        elapsed = 0 if p.returncode else float(out.strip())
        log(f'ELAPSED TIME____time: {elapsed}s')
        if elapsed > configuration.time_limit:
            raise TimeoutExpired('submition', configuration.time_limit)
        log(f'TEST RETURN CODE: {p.returncode}, ERR:{err}, OUT:{out}')
        return p.returncode, err, elapsed
    finally:
        os.nice(20)


def check_test(cid, test):
    p = Popen(f'docker cp {test.output.path} {cid}:ans.txt', shell=True)
    p.wait()
    log(f'CP OUTPUT: {p.returncode}')
    p = Popen(f'docker exec {cid} sh -c "diff output.txt ans.txt -Z -B -q"', shell=True)
    p.wait()
    log(f'DIFF: {p.returncode}')
    return p.returncode


def clear_task(cid):
    p = Popen(args=('docker', 'kill', cid))
    p.wait()
    log(f'DOCKER KILL:  {p.returncode}')


def run_submition_(submition, tests, conf, lang):
    try:
        elapsed = 0
        cid = prepare_task(submition, lang, conf)
        compiler_err = compile_task(lang, cid)
        if compiler_err:
            return 'CE', compiler_err, elapsed
        for test in tests:
            prepare_test(test, cid)
            try:
                ret_code, err_msg, el = run_test(cid, conf, lang)
                elapsed = max(elapsed, el)
                if ret_code:
                    if ret_code == 137:
                        return 'ML', None, elapsed
                    return 'RE', None, elapsed
                if check_test(cid, test):
                    return 'WA', None, elapsed
            except TimeoutExpired:
                log('TIME LIMIT')
                return 'TL', None, elapsed
        log('SUBMITION IS OK')
        return 'OK', None, elapsed
    except Exception as ex:
        log_ex(ex)
        return 'EX', str(ex), elapsed
    finally:
        clear_task(cid)


def prepare_arguments(submition):
    tests = list(submition.task.tests.all())
    tests.reverse()
    return submition, tests, submition.configuration, submition.configuration.language


def run_submition(submition):
    def cb(result):
        submition.result, submition.message, submition.elapsed_time = result
        submition.save()

    pool = ProcessPool(settings.TASK_TESTING_PROCESSES)
    pool.apply_async(run_submition_, prepare_arguments(submition), callback=cb)
