from django.db import models
from PIL import Image
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from .pathfinder import profile_pic_upload_path


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.png', upload_to=profile_pic_upload_path)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, **kwargs):
        super().save(kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        profile = Profile(user=instance)
        profile.save()


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
