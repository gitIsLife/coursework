def test_input_upload_path(instance, filename):
    return f'task/{instance.task_id}/tests/{instance.task.tests.count()}.in.txt'


def test_output_upload_path(instance, filename):
    return f'task/{instance.task_id}/tests/{instance.task.tests.count()}.out.txt'


def submition_upload_path(instance, filename):
    return f'user/{instance.user_id}/submitions/{instance.task_id}/' \
        f'{instance.user.submitions.filter(task_id=instance.task_id).count()}.py'


def profile_pic_upload_path(instance, filename):
    return f'profile_pics/{instance.user_id}/{filename}'
