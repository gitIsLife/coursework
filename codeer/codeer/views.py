from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import *
from .task_runner import run_submition
from .pathfinder import submition_upload_path
from .models import Submition
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from django.contrib import messages
from django.views.generic import (
    ListView,
    DetailView,
    UpdateView,
    DeleteView,
    CreateView
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Q, Count
import io


class TaskDetailView(LoginRequiredMixin, DetailView):
    model = Task
    template_name = 'tasks/task_info.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tests'] = Test.objects.filter(Q(public=True) & Q(task_id=self.kwargs['pk'])).order_by('id').all()
        return context


class TestCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Test
    template_name = 'tasks/new_test.html'
    form_class = TestForm

    def get_success_url(self):
        return reverse('new_test', kwargs={'pk': self.object.id})

    def test_func(self):
        task = Task.objects.get(pk=self.kwargs['pk'])
        return self.request.user == task.owner

    def get_initial(self):
        initials = super().get_initial()
        initials['user'] = self.request.user.id
        return initials


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = 'tasks/new_task.html'
    form_class = TaskForm

    def get_initial(self):
        initials = super().get_initial()
        initials['owner'] = self.request.user
        return initials

    def get_success_url(self):
        return reverse('new_test', kwargs={'pk': self.object.id})


class ConfigurationAddView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Configuration
    template_name = 'tasks/configuration_add.html'
    form_class = ConfigurationCreateForm

    def test_func(self):
        task = Task.objects.get(pk=self.kwargs['pk'])
        return self.request.user == task.owner

    def get_initial(self):
        initials = super().get_initial()
        initials['task'] = self.kwargs['pk']
        return initials

    def get_success_url(self):
        return reverse('configuration_add', kwargs={'pk': self.kwargs['pk']})


class ConfigurationView(ListView):
    model = Configuration
    template_name = 'tasks/configurations_view.html'
    context_object_name = 'configurations'
    ordering = ['id']
    paginate_by = 5

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(task__owner__id=self.request.user.id).filter(
            task__id=self.kwargs['pk'])
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['owner'] = Task.objects.get(pk=self.kwargs['pk']).owner_id == self.request.user.id
        return ctx


class ConfigurationDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Configuration
    success_url = '/my_tasks'
    template_name = 'tasks/configuration_confirm_delete.html'

    def test_func(self):
        conf = self.get_object()
        return self.request.user == conf.task.owner


@login_required
def submit_task(request, pk):
    if request.method == 'POST':
        form = SubmitionForm(request.POST, request.FILES)
        if form.is_valid():
            if not request.FILES:
                form.instance.solution.save(submition_upload_path(form.instance, request),
                                            io.StringIO(request.POST['editor']))
            form.save()
            run_submition(form.instance)
            return HttpResponseRedirect(reverse('submit_info', kwargs={'pk': form.instance.id}))
    else:
        form = SubmitionForm(initial={'task': pk, 'user': request.user.id})
    return render(request, 'tasks/submit_task.html', {'form': form, 'task': Task.objects.get(pk=pk)})


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'registration/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')

    u_form = UserUpdateForm(instance=request.user)
    p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'registration/profile.html', context)


class TaskView(ListView):
    model = Task
    template_name = 'tasks/task_list.html'
    context_object_name = 'tasks'
    ordering = ['-created']
    paginate_by = 9


class TaskViewByUser(ListView):
    model = Task
    template_name = 'tasks/user_tasks.html'
    context_object_name = 'tasks'
    ordering = ['-created']
    paginate_by = 9

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(owner=self.request.user)
        return queryset


class TestView(ListView):
    model = Test
    template_name = 'tasks/task_test_list.html'
    context_object_name = 'tests'
    ordering = ['id']
    paginate_by = 9

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(Q(task__owner__id=self.request.user.id) | Q(public=True)).filter(
            task__id=self.kwargs['pk'])
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['owner'] = Task.objects.get(pk=self.kwargs['pk']).owner_id == self.request.user.id
        return ctx


class TestDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Test
    success_url = '/my_tasks'
    template_name = 'tasks/test_confirm_delete.html'

    def test_func(self):
        test = self.get_object()
        return self.request.user == test.task.owner


class SubmitInfo(DetailView):
    model = Submition
    template_name = 'tasks/submition_detail.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset


class SubmittedTasksView(ListView):
    model = Task
    template_name = 'tasks/submitted_tasks.html'
    context_object_name = 'tasks'
    ordering = ['id']
    paginate_by = 9

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.annotate(
            submitions_count=Count('submitions', filter=Q(submitions__user_id=self.request.user.id))) \
            .filter(submitions_count__gte=1)
        return queryset


class SubmitionView(ListView):
    model = Submition
    template_name = 'tasks/task_submitions.html'
    context_object_name = 'submitions'
    ordering = ['id']
    paginate_by = 9

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user__id=self.request.user.id).filter(task__id=self.kwargs['pk'])
        return queryset


class TaskUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Task
    template_name = 'tasks/task_update.html'
    success_url = '/my_tasks'
    form_class = TaskForm

    def test_func(self):
        task = self.get_object()
        return self.request.user == task.owner


class TaskDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Task
    success_url = '/my_tasks'
    template_name = 'tasks/task_confirm_delete.html'

    def test_func(self):
        task = self.get_object()
        return self.request.user == task.owner


def download_test(request, id, input):
    test = Test.objects.get(pk=id)
    file = test.input if input else test.output
    if test.task.owner.id != request.user.id and not test.public:
        raise Http404
    with open(file.path, 'rb') as f:
        response = HttpResponse(f.read(), content_type='application/force-download')
        response['Content-Disposition'] = f'attachment; filename={file.name}'
        return response


def download_solution(request, id):
    submition = Submition.objects.get(pk=id)
    if submition.user.id != request.user.id:
        raise Http404
    with open(submition.solution.path, 'rb') as f:
        response = HttpResponse(f.read(), content_type='application/force-download')
        response['Content-Disposition'] = f'attachment; filename={submition.solution.name}'
        return response
