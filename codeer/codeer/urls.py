from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView


taskpatterns = [
    path('', views.TaskDetailView.as_view(), name='task'),
    path('new_test', views.TestCreateView.as_view(), name='new_test'),
    path('submit', views.submit_task, name='submit_task'),
    path('update', views.TaskUpdateView.as_view(), name='task_update'),
    path('tests', views.TestView.as_view(), name='task_test_list'),
    path('delete', views.TestDeleteView.as_view(), name='task_test_delete'),
    path('new_configuration', views.ConfigurationAddView.as_view(), name='configuration_add'),
    path('submitions', views.SubmitionView.as_view(), name='task_submitions'),
    path('configurations', views.ConfigurationView.as_view(), name='configurations_view')
]

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
    path('', include('django.contrib.auth.urls')),
    path('about_us', TemplateView.as_view(template_name='about_us.html'), name='about'),
    path('', views.TaskView.as_view(), name='task_list'),
    path('task/<int:pk>/', include(taskpatterns)),
    path('new_task/', views.TaskCreateView.as_view(), name='new_task'),
    path('my_tasks/', views.TaskViewByUser.as_view(), name='user_tasks'),
    path('submition/<int:pk>/', views.SubmitInfo.as_view(), name='submit_info'),
    path('submitions/', views.SubmittedTasksView.as_view(), name='submitted_tasks'),
    path('download_test/<int:id>/<int:input>', views.download_test, name='download_test'),
    path('download_solution/<int:id>', views.download_solution, name='download_solution'),
    path('submits/', views.SubmitionView.as_view(), name='task_submitions'),
    path('configuration/<int:pk>/delete', views.ConfigurationDeleteView.as_view(), name='configuration_delete')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
