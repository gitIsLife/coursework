from django.contrib import admin
from .user_models import Profile
from .models import Language, Configuration, Submition, Task


admin.site.register(Profile)
admin.site.register(Language)
admin.site.register(Configuration)
admin.site.register(Submition)
admin.site.register(Task)
